using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MGA.VPet
{
    public class BackgroundLayer : MonoBehaviour
    {
        [SerializeField] private ConfigureBackground bgController;
        [SerializeField] private float maxOpacity = 1f;

        public void Show()
        {
            LeanTween.value(gameObject, bgController.opacity, maxOpacity, 1f)
                .setOnUpdate(value =>
                {
                    bgController.opacity = value;
                });
        }
        
        public void Hide()
        {
            LeanTween.value(gameObject, maxOpacity, 0, .3f)
                .setOnUpdate(value =>
                {
                    bgController.opacity = value;
                });
        }

        public void Enable()
        {
            bgController.enabled = true;
        }
        
        public void Disable()
        {
            bgController.enabled = false;
        }
    }    
}

