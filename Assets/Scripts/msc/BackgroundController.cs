using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

namespace MGA.VPet.Legacy
{
    public class BackgroundController : MonoBehaviour
    {
        [SerializeField] private GameObject Background;
        [SerializeField] private BackgroundLayer layer1;
        [SerializeField] private BackgroundLayer layer2;
        
        [Button("ShowBackground")]
        public  void ShowBackground()
        {
            layer1.Show();
            layer2.Show();
        }
        
        [Button("HideBackground")]
        public  void HideBackground()
        {
            layer1.Hide();
            layer2.Hide();
        }

        [Button("Disable Background Controller")]
        public void DisableBackgroundController()
        {
            layer1.Disable();
            layer2.Disable();
        }
        
        [Button("Enable Background Controller")]
        public void EnableBackgroundController()
        {
            layer1.Enable();
            layer2.Enable();
        }
    }
    
}
