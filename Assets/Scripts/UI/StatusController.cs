using System.Collections;
using System.Collections.Generic;
using MGA.VPet.UI;
using UnityEngine;

namespace MGA.VPet.UI
{
    public class StatusController : MonoBehaviour
    {
        [SerializeField] private StatController healthUI;
        [SerializeField] private StatController hungerUI;
        [SerializeField] private StatController dirtUI;
        [SerializeField] private StatController loveUI;

        public void UpdateStatus(int health, int hunger, int dirt, int love)
        {
            healthUI.UpdateValue(health);
            hungerUI.UpdateValue(hunger);
            dirtUI.UpdateValue(dirt);
            loveUI.UpdateValue(love);

            UpdateUI();
        }

        public void UpdateUI()
        {
            healthUI.UpdateFill();
            hungerUI.UpdateFill();
            dirtUI.UpdateFill();
            loveUI.UpdateFill();
        }
    }    
}