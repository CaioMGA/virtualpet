using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class TitleScreenController : MonoBehaviour
{
    [SerializeField] private CanvasGroup Egg;
    [SerializeField] private CanvasGroup title;
    [SerializeField] private CanvasGroup tapToStart;
    [SerializeField] private CanvasGroup soundTest;
    [SerializeField] private CanvasGroup artGallery;
    [SerializeField] private CanvasGroup about;
    [SerializeField] private CanvasGroup curtainBlack;
    [SerializeField] private CanvasGroup curtainWhite;
    [SerializeField] private GameObject tapTrigger;
    [Space] [SerializeField] private GameObject CreditsPanel;

    private int _eggAnim;
    private int _tapAnim;
    private int _flashAnim;

    private void Awake()
    {
        Init();
    }

    private void Start()
    {
        ShowEgg();
        Invoke(nameof(ShowTitle), 2f);
        Invoke(nameof(AnimateEgg), 3f);
        Invoke(nameof(ShowSoundTestButton), 5f);
        Invoke(nameof(ShowArtGalleryButton), 5f);
        Invoke(nameof(ShowAboutButton), 5f);
        Invoke(nameof(AnimateTapToStart), 6f);
    }

    public void OnTap()
    {
        tapTrigger.SetActive(false);
        HideTexts();
        Invoke(nameof(CenterEgg), 1f);
        Invoke(nameof(Flash), 4f);
        Invoke(nameof(Flash), 6f);
        Invoke(nameof(Flash), 8f);
        Invoke(nameof(Flash), 8.5f);
        Invoke(nameof(WhiteScreen), 9f);
        Invoke(nameof(GoToGameScene), 9.01f);
    }

    [Button("Go to Game")]
    public void GoToGameScene()
    {
        SceneManager.LoadScene("Game");
    }

    [Button("Init")]
    private void Init()
    {
        Egg.alpha = 0;
        title.alpha = 0;
        tapToStart.alpha = 0;
        soundTest.alpha = 0;
        artGallery.alpha = 0;
        about.alpha = 0;
        curtainBlack.alpha = 0;
        curtainWhite.alpha = 0;
        tapTrigger.SetActive(false);
    }

    [Button("Show Egg")]
    private void ShowEgg()
    {
        LeanTween.alphaCanvas(Egg, 1, 2f);
    }

    [Button("ShowSoundTestButton")]
    private void ShowSoundTestButton()
    {
        LeanTween.alphaCanvas(soundTest, 1, .5f);
    }

    [Button("ShowAboutButton")]
    private void ShowAboutButton()
    {
        LeanTween.alphaCanvas(about, 1, .5f);
    }
    
    [Button("ShowArtGalleryButton")]
    private void ShowArtGalleryButton()
    {
        LeanTween.alphaCanvas(artGallery, 1, .5f);
    }

    [Button("Animate Egg")]
    private void AnimateEgg()
    {
        _eggAnim = LeanTween.rotateLocal(Egg.gameObject, Vector3.forward * 2, .1f)
            .setDelay(1f)
            .setOnComplete(() =>
            {
                LeanTween.rotateLocal(Egg.gameObject, Vector3.forward * -2, .1f)
                    .setOnComplete(AnimateEgg);
            }).id;
    }

    [Button("Show Title")]
    private void ShowTitle()
    {
        LeanTween.alphaCanvas(title, 1, 1f);
    }

    [Button("Blink Tap To Start")]
    private void AnimateTapToStart()
    {
        _tapAnim = LeanTween.alphaCanvas(tapToStart, 1, 1f).setLoopPingPong().id;
        tapTrigger.SetActive(true);
    }

    [Button("Flash")]
    private void Flash()
    {
        if (_flashAnim != -1)
        {
            LeanTween.cancel(_flashAnim);
            _flashAnim = -1;
        }

        curtainWhite.alpha = 1;
        _flashAnim = LeanTween.alphaCanvas(curtainWhite, 0f, .3f)
            .setEase(LeanTweenType.easeInQuart).id;
    }

    [Button("WhiteScreen")]
    private void WhiteScreen()
    {
        if (_flashAnim != -1)
        {
            LeanTween.cancel(_flashAnim);
            _flashAnim = -1;
        }

        curtainWhite.alpha = 1;
    }

    [Button("Fade Out")]
    private void FadeOut()
    {
        curtainBlack.alpha = 0;
        LeanTween.alphaCanvas(curtainBlack, 1f, 1f);
    }

    [Button("Fade In")]
    private void FadeIn()
    {
        curtainBlack.alpha = 1;
        
        LeanTween.alphaCanvas(curtainBlack, 0f, 1f);
    }

    [Button("HideTexts")]
    private void HideTexts()
    {
        LeanTween.cancel(_tapAnim);
        LeanTween.alphaCanvas(tapToStart, 0, .5f);
        LeanTween.alphaCanvas(title, 0, .5f);
        // LeanTween.alphaCanvas(soundTest, 0, .5f);
        // LeanTween.alphaCanvas(artGallery, 0, .5f);
        // LeanTween.alphaCanvas(about, 0, .5f);
        
        about.gameObject.SetActive(false);
        artGallery.gameObject.SetActive(false);
        soundTest.gameObject.SetActive(false);
    }

    [Button("CenterEgg")]
    private void CenterEgg()
    {
        LeanTween.moveLocalY(Egg.gameObject, 0, .5f);
    }

    public void ShowSoundTest()
    {
        SceneManager.LoadScene("SoundTest");
    }

    public void ShowArtGallery()
    {
        SceneManager.LoadScene("ArtGallery");
    }

    public void OpenCaioSite()
    {
        Application.OpenURL("www.caiomga.com.br");
    }

    public void ShowAboutPanel()
    {
        CreditsPanel.SetActive(true);
    }
    
    public void HideAboutPanel()
    {
        CreditsPanel.SetActive(false);
    }
    
}
