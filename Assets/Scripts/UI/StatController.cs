using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;

namespace MGA.VPet.UI
{
    public class StatController : MonoBehaviour
    {
        [SerializeField] private Image fillImage;
        public int value;
        public int MAXvalue;

        public void UpdateValue(int v)
        {
            value = v;
        }

        [Button("UpdaetFill")]
        public void UpdateFill()
        {
            fillImage.fillAmount = value / (float)MAXvalue;
        }
        
        
    }
}