using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using UnityEngine;
using UnityEngine.UI;

namespace MGA.VPet.SoundTest
{
    public class MultiStatesButton : MonoBehaviour
    {
        [SerializeField] private List<BtnState> states;

        public void ShowState(string state)
        {
            foreach (var stt  in states)
            {
                stt.icon.SetActive(stt.name == state);
            }
        }
    }

    [Serializable]
    public class BtnState
    {
        public GameObject icon;
        public string name;
    }
    
}
