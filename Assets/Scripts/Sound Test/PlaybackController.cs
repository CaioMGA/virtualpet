using System;
using System.Collections;
using System.Collections.Generic;
using MGA.VPet.SoundTest;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MGA.VPet.SoundTest
{
    public class PlaybackController : MonoBehaviour
    {
        [SerializeField] private Playlist Playlist;
        [Space]
        [SerializeField] private MultiStatesButton playBtn;
        [SerializeField] private Button stopBtn;
        [SerializeField] private Button prevBtn;
        [SerializeField] private Button nextBtn;
        [SerializeField] private MultiStatesButton repeatBtn;
        [Space]
        [SerializeField] private AudioSource audioSource;

        [SerializeField] private TextMeshProUGUI artistName;
        [SerializeField] private TextMeshProUGUI songName;
        [SerializeField] private Slider timelineSlider;

        public Action OnPauseAction;
        public Action OnPlayAction;
        public Action OnStopAction;

        private bool isPlaying = false;
        private bool isRepeating = false;
        private bool repeatOne = false;
        private bool repeatAll = false;


        private void Start()
        {
            var song = Playlist.GetNext();
            SetupButtons();
            SetupNewSong(song, false);
        }

        private void FixedUpdate()
        {
            timelineSlider.value = audioSource.time / audioSource.clip.length;
        }

        private void LateUpdate()
        {
            if (audioSource.time <= 0)
            {
                if (isPlaying)
                {
                    if (!repeatAll) return;
                    
                    repeatAll = false;
                    Invoke(nameof(RepeatHack), .5f);
                    PBNext(true);
                }
            }
        }

        private void RepeatHack()
        {
            repeatAll = true;
        }
        
        private void SetupButtons()
        {
            playBtn.GetComponent<Button>().onClick.AddListener(PBPlayPause);
            repeatBtn.GetComponent<Button>().onClick.AddListener(PBRepeat);
            
            stopBtn.onClick.AddListener(PBStop);
            prevBtn.onClick.AddListener(PBPrev);
            nextBtn.onClick.AddListener(() =>
            {
                PBNext();
            });
        }
        private void SetupNewSong(SongInfo songInfo, bool playInstantly = false)
        {
            var isClipPlaying = audioSource.isPlaying;
            audioSource.Stop();
            audioSource.clip = songInfo.clip;
            if(isClipPlaying || playInstantly) audioSource.Play();
            isPlaying = isClipPlaying || playInstantly;
            artistName.text = songInfo.artist;
            songName.text = songInfo.songName;
        }

        private void PBPlayPause()
        {
            if (audioSource.isPlaying)
            {
                audioSource.Pause();
                playBtn.ShowState("PAUSE");
                OnPauseAction?.Invoke();
            }
            else
            {
                audioSource.Play();
                playBtn.ShowState("PLAY");
                OnPlayAction?.Invoke();
            }
        }
        
        private void PBStop()
        {
            audioSource.Stop();
            playBtn.ShowState("PAUSE");
            OnStopAction?.Invoke();
        }
        
        private void PBPrev()
        {
            
            if (repeatAll)
            {
                repeatAll = false;
                Invoke(nameof(RepeatHack), .2f);    
            }
            
            var song = Playlist.GetPrev();
            SetupNewSong(song);
        }
        
        private void PBNext(bool playInstantly = false)
        {

            if (repeatAll)
            {
                repeatAll = false;
                Invoke(nameof(RepeatHack), .2f);    
            }
            
            
            var song = Playlist.GetNext();
            SetupNewSong(song, playInstantly);
        }
        
        private void PBRepeat()
        {
            if (!isRepeating)
            {
                isRepeating = true;
                repeatOne = true;
                repeatBtn.ShowState("REPEAT_ONE");
                audioSource.loop = true;
            }
            else
            {
                if (repeatOne)
                {
                    repeatOne = false;
                    repeatAll = true;
                    repeatBtn.ShowState("REPEAT_ALL");
                    audioSource.loop = false;
                }
                else
                {
                    isRepeating = false;
                    repeatOne = false;
                    repeatAll = false;
                    repeatBtn.ShowState("NO_REPEAT");
                    audioSource.loop = false;
                }
            }
        }
    }
}