using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MGA.VPet.SoundTest
{

    [CreateAssetMenu(menuName = "VPet/Playlist", fileName = "new Playlist")]
    public class Playlist : ScriptableObject
    {
        public List<SongInfo> songs;

        private int _curSong = -1;
        
        public SongInfo GetCurrentSong()
        {
            return songs[_curSong];
        }
        
        public SongInfo GetNext()
        {
            _curSong++;
            if (_curSong >= songs.Count) _curSong = 0;
            return GetCurrentSong();
        }
        
        public SongInfo GetPrev()
        {
            _curSong--;
            if (_curSong < 0) _curSong = songs.Count - 1;
            return GetCurrentSong();
        }
    }

    [Serializable]
    public class SongInfo
    {
        public string artist;
        public string songName;
        public AudioClip clip;
    }
}