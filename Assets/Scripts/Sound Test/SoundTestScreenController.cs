using System.Collections;
using System.Collections.Generic;
using MGA.VPet.Legacy;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace MGA.VPet.SoundTest
{
    public class SoundTestScreenController : MonoBehaviour
    {
        [SerializeField] private BackgroundController backgroundController;
        [SerializeField] private PlaybackController playbackController;

        private void Start()
        {
            playbackController.OnPauseAction = backgroundController.DisableBackgroundController;
            playbackController.OnPlayAction = () =>
            {
                backgroundController.EnableBackgroundController();
                backgroundController.ShowBackground();

            };
            playbackController.OnStopAction = backgroundController.HideBackground;
        }
        
        public void OnBackButtonPress()
        {
            SceneManager.LoadScene("TitleScreen");
        }

    }    
}