using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MGA.VPet.SimplePet
{
    public class TutorialController : MonoBehaviour
    {
        [SerializeField] private List<GameObject> tutorialSteps;
        [SerializeField] private GameObject tutorialContainer;
        private int _curStep = 0;

        private Action OnTutorialComplete;
        
        public void ShowTutorial()
        {
            _curStep = -1;
            tutorialContainer.SetActive(true);
            NextStep();
        }

        public void NextStep()
        {
            var prevStep = _curStep;
            _curStep++;
            if(prevStep >= 0)
                tutorialSteps[prevStep].SetActive(false);
            
            if (_curStep >= tutorialSteps.Count)
            {
                OnTutorialComplete?.Invoke();   
                tutorialContainer.SetActive(false);
                return;
            }
            
            tutorialSteps[_curStep].SetActive(true);
        }

        public void SetOnCompleteTutorialCallback(Action callback)
        {
            OnTutorialComplete = callback;
        }
    }
}