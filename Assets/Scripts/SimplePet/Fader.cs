using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MGA.VPet.SimplePet
{
    public class Fader : MonoBehaviour
    {
        private static Fader instance;
        private CanvasGroup cg;

        private void Awake()
        {
            instance = this;
            cg = GetComponent<CanvasGroup>();
        }
        
        public static void FadeIn()
        {
            instance.cg.alpha = 1;
            LeanTween.alphaCanvas(instance.cg, 0, 1f);
        }
        
        public static void FadeOut()
        {
            instance.cg.alpha = 0;
            LeanTween.alphaCanvas(instance.cg, 1, 1f);
        }
    }
}