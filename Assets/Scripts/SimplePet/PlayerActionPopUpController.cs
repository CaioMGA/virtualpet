using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

namespace MGA.VPet.SimplePet
{
    public class PlayerActionPopUpController : MonoBehaviour
    {
        [SerializeField] private GameObject Caress;
        [SerializeField] private GameObject Play;
        [SerializeField] private GameObject Medicine;
        [SerializeField] private GameObject Feed;
        [SerializeField] private GameObject Clean;
        [SerializeField] private GameObject Wash;
        [Space]
        [SerializeField] private GameObject Popup;
        [SerializeField] private GameObject Content;
        [SerializeField] private CanvasGroup CanvasGroup;

        [SerializeField] private GameObject _selectedAnimation;
        private float offscreenY = 0;
        private float _popUpTime = 1f;
        private bool showing = false;

        private static PlayerActionPopUpController instance;

        private void Start()
        {
            instance = this;
        }
        
        [Button("Show Medicine")]
        public static void ShowMedicine()
        {
            instance._selectedAnimation = instance.Medicine;
            instance._popUpTime = 2.5f;
            instance.Show();
        }
        
        [Button("Show Feed")]
        public static void ShowFeed()
        {
            instance._selectedAnimation = instance.Feed;
            instance._popUpTime = 2.5f;
            instance.Show();
        }
        
        private void Show()
        {
            Popup.SetActive(true);
            if (showing) return;

            showing = true;
            
            LeanTween.alphaCanvas(CanvasGroup, 1, .5f)
                .setOnComplete(ShowPopup);
        }

        private void ShowPopup()
        {
            offscreenY = Content.transform.localPosition.y;
            LeanTween.moveLocalY(Content, 0, .3f)
                .setEase(LeanTweenType.easeOutBack);
            
            _selectedAnimation.SetActive(true);
            
            Invoke(nameof(Close), _popUpTime);
        }

        private void Close()
        {
            if (!showing) return;
            
            showing = false;
            
            LeanTween.moveLocalY(Content, offscreenY, .3f)
                .setEase(LeanTweenType.easeInBack);
            LeanTween.alphaCanvas(CanvasGroup, 0, .5f)
                .setOnComplete(DisablePopup);
        }

        private void DisablePopup()
        {
            _selectedAnimation.SetActive(false);
            Popup.SetActive(false);
        }
        
        
    }
}