using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace MGA.VPet.SimplePet
{
    [Serializable]
    public class ScheduledEvent
    {
        public int Tick; // time to execute the event in Ticks
        public ScheduleEventType Type;

    }

    public enum ScheduleEventType
    {
        Sleep = 1,
        Poop = 2,
        WakeUp = 3,
        SuddenSickness = 4,
        Caress = 5,
        Play = 6,
        Feed = 7,
        Medicine = 8,
        Wash = 9,
        Clean = 10
        
        
    }

}
