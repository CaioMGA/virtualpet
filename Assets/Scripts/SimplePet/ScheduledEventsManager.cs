using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MGA.VPet.SimplePet
{
    public class ScheduledEventsManager : MonoBehaviour
    {
        // may want to separate lists by event type
        public List<ScheduledEventsCollection> ScheduledEventsCollections;
        public List<ScheduledEventsCollection> ScheduledPlayerActions;

        private static ScheduledEventsManager _instance;
        [SerializeField] private List<ScheduledEvent> _schedule;

        private void CreateSchedule()
        {
            _schedule = new List<ScheduledEvent>();
            foreach (var collection in ScheduledEventsCollections)
            {
                _schedule.AddRange(collection.Events);
            }
            
            foreach (var playerAction in ScheduledPlayerActions)
            {
                _schedule.AddRange(playerAction.Events);
            }

            _schedule = _schedule.OrderBy(x => x.Tick).ToList();

        }

        private void Awake()
        {
            CreateSchedule();
            _instance = this;
        }

        public static ScheduledEvent CheckSchedule(int tick)
        {
            if (_instance._schedule.Count < 1) return null;
            if (_instance._schedule[0].Tick != tick) return null;
            
            var scheduledEvent = _instance._schedule[0];
            _instance._schedule.RemoveAt(0);
            return scheduledEvent;
        }
    }
}