using System.Collections;
using System.Collections.Generic;
using UnityEditor.Timeline.Actions;
using UnityEngine;
using UnityEngine.Rendering;

namespace MGA.VPet.Music
{
    public class MusicController : MonoBehaviour
    {
        [SerializeField] private List<GameObject> Songs; 
        private static MusicController instance;

        public static void Play(MusicBg song)
        {
            for (int i = 0; i < instance.Songs.Count; i++)
            {
                instance.Songs[i].SetActive(i == (int)song);
            }
        }

       private void Awake()
        {
            instance = this;
        }
    }
    
    public enum MusicBg {
        TitleScreen = 0,
        LevelUp = 1,
        Sleeping = 2,
        BadEnding = 3,
        GoodEnding = 4,
        Credits = 5,
        WakeUp = 6
            
    }
}