using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MGA.VPet.SimplePet
{
    [CreateAssetMenu(fileName = "ScheduledEvents", menuName = "VPet/SimplePet/Scheduled Events Collection")]
    public class ScheduledEventsCollection : ScriptableObject
    {
        public List<ScheduledEvent> Events;

    }
}

