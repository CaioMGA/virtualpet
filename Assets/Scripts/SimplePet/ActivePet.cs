using System.Collections;
using System.Collections.Generic;
using MGA.VPet.Music;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.Serialization;

namespace MGA.VPet.SimplePet
{
    public class ActivePet : MonoBehaviour
    {
        public StatData dirt;
        public StatData health;
        public StatData hunger;
        public StatData love;

        public int sleepingTimerCurrent = 0;

        public bool dirty = false;
        public bool sick = false;
        public bool hungry = false;
        public bool loveless = false;
        public bool poopOnScreen = false;

        public int lvl = 0;
        
        public string curState = "IDLE";
        
        // Flags
        public bool sleeping = false;
        public bool poop = false;

        public bool YOUWIN = false;

        public bool alive = false;

        public void Init(StatTable data)
        {
            dirt = data.dirt;
            health = data.health;
            hunger = data.hunger;
            love = data.love;

            dirt.Init();
            health.Init();
            hunger.Init();
            love.Init();
        }

        public void ExecuteScheduledEvent(ScheduledEvent scheduledEvent)
        {
            switch (scheduledEvent.Type)
            {
                case ScheduleEventType.Caress:
                    Caress();
                    break;
                case ScheduleEventType.Play:
                    Play();
                    break;
                case ScheduleEventType.Feed:
                    Feed();
                    break;
                case ScheduleEventType.Medicine:
                    Medicine();
                    break;
                case ScheduleEventType.Poop:
                    Poop();
                    break;
                case ScheduleEventType.Clean:
                    Clean();
                    break;
                case ScheduleEventType.Wash:
                    Wash();
                    break;
                case ScheduleEventType.Sleep:
                    Sleep();
                    break;
                case ScheduleEventType.WakeUp:
                    WakeUp();
                    break;
            }
        }

        [Button("Poop")]
        private void Poop()
        {
            Fader.FadeIn();
            BackgroundController.ShowPoop();
            poopOnScreen = true;
            Debug.Log("Pooped");
        }
        
        [Button("Sleep")]
        private void Sleep()
        {
            Fader.FadeIn();
            sleeping = true;
            MusicController.Play(MusicBg.Sleeping);
            BackgroundController.ShowSleeping();
            Debug.Log("Sleeping");
        }
        
        [Button("WakeUp")]
        private void WakeUp()
        {
            Fader.FadeIn();
            sleeping = false;
            MusicController.Play(MusicBg.WakeUp);
            BackgroundController.ShowIdle();
            Debug.Log("Woke up");
        }
        
        [Button("LEVEL UP")]
        public void LevelUp()
        {
            lvl++;
            // TO DO: fanfares and all the good stuff
            if (lvl == 4)
            {
                YOUWIN = true;
            }
        }
        
        [Button("Caress")]
        public void Caress()
        {
            if (poopOnScreen || dirty || sleeping)
            {
                //TO DO: Denied sound
                return;
            }
            love.value++;
            love.value++;
            if (love.value > love.max)
            {
                love.value = love.max;
            }
        }
        
        [Button("Play")]
        public void Play()
        {
            if (sick || sleeping)
            {
                //TO DO: Denied sound
                return;
            }
            dirt.value--;
            if (poopOnScreen)
                dirt.value--;
            health.value++;
            if (health.value > health.max)
            {
                health.value = health.max;
            }
            hunger.value--;
        }
        
        [Button("Medicine")]
        public void Medicine()
        {
            if (poopOnScreen || sleeping)
            {
                //TO DO: Denied sound
                return;
            }

            PlayerActionPopUpController.ShowMedicine();
            health.value = health.max;
        }
        
        [Button("Feed")]
        public void Feed()
        {
            if (sick || dirty || poopOnScreen || sleeping)
            {
                //TO DO: Denied sound
                return;
            }
            PlayerActionPopUpController.ShowFeed();
            
            if (hunger.value >= hunger.max)
            {
                dirt.value--;
            }
            else
            {
                hunger.value++;
                hunger.value++;
                hunger.value++;
                if (hunger.value > hunger.max)
                {
                    hunger.value = hunger.max;
                }
            }
        }
        [Button("Wash")]
        public void Wash()
        {
            if (sleeping) return;
            dirt.value = dirt.max;
        }
        
        [Button("Clean")]
        public void Clean()
        {
            if (!poopOnScreen || sleeping)
            {
                //TO DO: Denied sound
                return;
            }
            poopOnScreen = false;
            Fader.FadeIn();
            BackgroundController.ShowIdle();
            Debug.Log("Poo is removed");
        }

        public string GetCurrentState()
        {
            if (sleeping)
            {
                SetSleeping();
                return curState;
            }
            if (dirt.value <= dirt.alert)
            {
                SetDirty();
                return curState;
            }

            if (hunger.value <= hunger.alert)
            {
                SetHungry();
                return curState;
            }

            if (health.value <= health.alert)
            {
                SetSick();
                return curState;
            }

            if (love.value <= love.alert)
            {
                SetLoveless();
                return curState;
            }
            
            SetIdle();
            return curState;
        }

        private void SetDirty()
        {
            curState = "DIRTY";
            dirty = true;
            sick = false;
            hungry = false;
            loveless = false;
        }
        
        private void SetSleeping()
        {
            curState = "SLEEPING";
            dirty = false;
            sick = false;
            hungry = false;
            loveless = false;
        }

        private void SetSick()
        {
            curState = "SICK";
            dirty = false;
            sick = true;
            hungry = false;
            loveless = false;
        }
        
        private void SetHungry()
        {
            curState = "HUNGRY";
            dirty = false;
            sick = false;
            hungry = true;
            loveless = false;
        }
        
        private void SetLoveless()
        {
            curState = "LOVELESS";
            dirty = false;
            sick = false;
            hungry = false;
            loveless = true;
        }
        
        private void SetIdle()
        {
            curState = "IDLE";
            dirty = false;
            sick = false;
            hungry = false;
            loveless = false;
            sleeping = false;
        }

    }
}