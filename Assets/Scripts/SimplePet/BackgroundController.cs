using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MGA.VPet.SimplePet
{
    public class BackgroundController : MonoBehaviour
    {
        public GameObject Idle;
        public GameObject Loveless;
        public GameObject Love;
        public GameObject Sick;
        public GameObject Hungry;
        public GameObject Sleeping;
        public GameObject LevelUp;
        public GameObject YouLose;
        public GameObject Poop;

        private static BackgroundController instance;

        private void Awake()
        {
            instance = this;
        }
        
        private static void HideEverybody()
        {
            instance.Idle.SetActive(false);
            instance.Loveless.SetActive(false);
            instance.Love.SetActive(false);
            instance.Sick.SetActive(false);
            instance.Hungry.SetActive(false);
            instance.Sleeping.SetActive(false);
            instance.LevelUp.SetActive(false);
            instance.YouLose.SetActive(false);
            instance.Poop.SetActive(false);
        }
        
        public static void ShowIdle(){
            HideEverybody();
            instance.Idle.SetActive(true);
        }
        
        public static void ShowLoveless(){
            HideEverybody();
            instance.Loveless.SetActive(true);
        }
        
        public static void ShowLove(){
            HideEverybody();
            instance.Love.SetActive(true);
        }
        
        public static void ShowSick(){
            HideEverybody();
            instance.Sick.SetActive(true);
        }
        
        public static void ShowHungry(){
            HideEverybody();
            instance.Hungry.SetActive(true);
        }
        
        public static void ShowSleeping(){
            HideEverybody();
            instance.Sleeping.SetActive(true);
        }
        
        public static void ShowLevelUp(){
            HideEverybody();
            instance.LevelUp.SetActive(true);
        }
        
        public static void ShowYouLose(){
            HideEverybody();
            instance.YouLose.SetActive(true);
        }
        
        public static void ShowPoop(){
            HideEverybody();
            instance.Poop.SetActive(true);
        }
        
    }
}
