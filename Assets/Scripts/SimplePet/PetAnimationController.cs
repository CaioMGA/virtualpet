using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityEngine;
using NaughtyAttributes;
using Unity.Collections;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.XR;


namespace MGA.VPet.Animations
{

    public class PetAnimationController : MonoBehaviour
    {
        [SerializeField] private List<PetPieces> petStates;
        [SerializeField] private PetImagesOnScreen petImagesOnScreen;

        [FormerlySerializedAs("_curState")] public  PetPieces curState;
        [SerializeField] private int _curStateIndex = 0;

        [Button("UpdateSprites")]
        public void UpdateSprites()
        {
            petImagesOnScreen.Base.sprite = curState.Base;
            
            petImagesOnScreen.EyesAnimation.sprites[0].Sprite =curState.EyesOpen;
            petImagesOnScreen.EyesAnimation.sprites[1].Sprite =curState.EyesClosed;
            petImagesOnScreen.EyesAnimation.Animate();
            
            petImagesOnScreen.Mouth.sprite = curState.Mouth;
            petImagesOnScreen.Cap.sprite = curState.Cap;

            petImagesOnScreen.SickOverlay.SetActive(curState.sickOverlayOn);
            petImagesOnScreen.DirtyOverlay.SetActive(curState.dirtyOverlayOn);
            petImagesOnScreen.SleepingMouth.SetActive(curState.isSleeping);

        }

        [Button("Next State")]
        public void NextState()
        {
            _curStateIndex++;
            if (_curStateIndex >= petStates.Count) _curStateIndex = 0;
            curState = petStates[_curStateIndex];
            UpdateSprites();
        }

        public void PrevState()
        {
            _curStateIndex--;
            if (_curStateIndex < 0 ) _curStateIndex = petStates.Count - 1;
            curState = petStates[_curStateIndex];
            UpdateSprites();
        }

        public void ChangeState(string stt)
        {
            foreach (var petState in petStates.Where(petState => petState.name.Equals(stt)))
            {
                curState = petState;
                UpdateSprites();
                return;
            }
        }
        
        public string GetCurState()
        {
            return curState.name;
        }

        [Button("ShowHungry")]
        private void ShowHungry()
        {
            ChangeState("HUNGRY");
        }
        
        [Button("ShowSICK")]
        private void ShowSick()
        {
            ChangeState("SICK");
        }
        
        [Button("ShowLOVELESS")]
        private void ShowLoveless()
        {
            ChangeState("LOVELESS");
        }
        
        [Button("ShowIDLE")]
        private void ShowIdle()
        {
            ChangeState("IDLE");
        }
        
        [Button("ShowDIRTY")]
        private void ShowDirty()
        {
            ChangeState("DIRTY");
        }
        
        [Button("ShowSLEEPING")]
        private void ShowSleeping()
        {
            ChangeState("SLEEPING");
        }
    }

    [Serializable]
    public class PetPieces
    {
        public string name = "";
        public Sprite Base;
        public Sprite EyesClosed;
        public Sprite EyesOpen;
        public Sprite Mouth;
        public Sprite Cap;
        public bool sickOverlayOn = false;
        public bool dirtyOverlayOn = false;
        public bool isSleeping = false;
    }
    
    [Serializable]
    public class PetImagesOnScreen
    {
        public Image Base;
        public Image Eyes;
        public SpriteSwapLoop EyesAnimation;
        public Image Mouth;
        public Image Cap;
        public GameObject SickOverlay;
        public GameObject DirtyOverlay;
        public GameObject SleepingMouth;

    }
    
    
}
