using System;
using System.Collections;
using System.Collections.Generic;
using MGA.VPet.Animations;
using TMPro;
using UnityEngine;
using NaughtyAttributes;

namespace MGA.VPet.SimplePet
{
    public class PetDisplayController : MonoBehaviour
    {
        public List<GameObject> Lvls;
        public List<GameOverStates> GameOverStates;

        public int curLevel;
        public string curState = "IDLE";
        public string gameoverState = "VICTORY";
        public bool isAlive = true;

        public PetAnimationController petAnimationController;
        public CanvasGroup PlayerActionsCanvasGroup;

        private void Start()
        {
            UpdateDisplay();
        }

        public void SetCurState(string stt)
        {
            curState = stt;
        }

        public void UpdateReferences()
        {
            petAnimationController = Lvls[curLevel].GetComponent<PetAnimationController>();
        }
        
        [Button("LevelUp")]
        public void LevelUp()
        {
            curLevel++;
            
            if (curLevel >= Lvls.Count)
            {
                curLevel = 0;
            }
            
            if (curLevel == 0 || curLevel == 4)
            {
                UpdateDisplay();
                return;
            }
            petAnimationController = Lvls[curLevel].GetComponent<PetAnimationController>();
            UpdateDisplay();
        }

        [Button("LevelDown")]
        public void LevelDown()
        {
            curLevel--;
            if (curLevel < 0)
            {
                curLevel = Lvls.Count - 1;
            }

            if (curLevel == 0 || curLevel == 4)
            {
                UpdateDisplay();
                return;
            }

            petAnimationController = Lvls[curLevel].GetComponent<PetAnimationController>();
            petAnimationController.ChangeState(curState);
            
            UpdateDisplay();
        }
        
        [Button("UpdateDisplay")]
        public void UpdateDisplay()
        {
            petAnimationController.ChangeState(curState);
            
            for (var i = 0; i < Lvls.Count; i++)
            {
                 Lvls[i].SetActive(i == curLevel && isAlive);
            }

            foreach (var goState in GameOverStates)
            {
                if (isAlive)
                {
                    goState.Obj.SetActive(false);
                }
                else
                {
                    goState.Obj.SetActive(gameoverState == goState.name);
                    PlayerActionsCanvasGroup.alpha = 0;
                }
            }
        }
        
        [Button("NextState")]
        public void NextState()
        {
            if (curLevel == 0 || curLevel == 4) return;
            
            petAnimationController.NextState();
        }
        
        [Button("PrevState")]
        public void PrevState()
        {
            if (curLevel == 0 || curLevel == 4) return;
            
            petAnimationController.PrevState();
        }

        public void SetGameOver(string stat)
        {
            gameoverState = stat;
            isAlive = false;
            UpdateDisplay();
        }
    }

    [Serializable]
    public class GameOverStates
    {
        public string name = "";
        public GameObject Obj;
    }
}