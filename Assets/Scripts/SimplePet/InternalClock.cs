using System;
using System.Collections;
using System.Collections.Generic;
using MGA.VPet.Music;
using MGA.VPet.UI;
using UnityEngine;
using NaughtyAttributes;
using UnityEditor;
using UnityEngine.Serialization;
using UnityEngine.UIElements;

namespace MGA.VPet.SimplePet
{
    public class InternalClock : MonoBehaviour
    {
        [SerializeField] [ReadOnly] private int tickCounter = 0;

        [FormerlySerializedAs("_statTable")] [SerializeField]
        private StatTable statTable;

        [FormerlySerializedAs("_activePet")] [SerializeField]
        private ActivePet activePet;

        [SerializeField] private BackgroundController bgController;
        [Space]

        [SerializeField] private float tickRateInMilliseconds  = 100f;
        [SerializeField] private StatusController statusController;
        [SerializeField] private PetDisplayController displayController;
        [SerializeField] private CanvasGroup Flash;
        [SerializeField] private CanvasGroup topUI;
        [SerializeField] private CanvasGroup bottomUI;
        [Space] [SerializeField] private TutorialController tutorialController;
        private float _tickTime = 0;
        private float _curTickTime = 0;
        [Space]
        // Flags
        [SerializeField] private bool _autoplay = false;
        [SerializeField] private bool _dontUpdateui = false;
        public bool _ticking = false;
        public bool invincible = false;
        public bool skipTutorial = false;
        
        private void Awake()
        {
            Flash.alpha = 1;
        }
        
        private void Start()
        {
            _tickTime = tickRateInMilliseconds / 1000f;
            LeanTween.alphaCanvas(Flash, 0, 2f);
            Invoke(nameof(Hatch), 0f);
            
            Invoke(nameof(ShowTopUI), 4f);
            Invoke(nameof(ShowBottomUI), 5f);

            if (!skipTutorial)
            {
                Invoke(nameof(ShowTutorial), 6f);
            }
            
            InitStats();
            
            if (_ticking)
            {
                Tick();    
            }
        }
        private void Update()
        {
            // auto play does one tick every  FixedUpdate() call
            if (_autoplay) return;

            if (!_ticking) return;
            
            if (activePet.YOUWIN) return;
            
            _curTickTime += Time.deltaTime;
            if (!(_curTickTime >= _tickTime)) return;
            Tick();
            _curTickTime -= _tickTime;

        }

        private void FixedUpdate()
        {
            // auto play does one tick every single FixedUpdateCall
            if (!_autoplay) return;
            
            if (!_ticking) return;

            if (activePet.YOUWIN) return;
            
            Tick();
        }

        private void InitStats()
        {
            activePet.Init(statTable);
        }
        
        [Button("TICK")]
        private void Tick()
        {
            tickCounter++;
            if(!invincible)
                UpdatePetStatus();
            
            CheckGameOver();
            if (activePet.alive)
            {
                var scheduledEvent = ScheduledEventsManager.CheckSchedule(tickCounter);
                if (scheduledEvent != null)
                {
                    activePet.ExecuteScheduledEvent(scheduledEvent);
                }
            }

            if(!_dontUpdateui)
                UpdateUI();
            CheckLevelUp();
            UpdateDisplay();
        }

        private void ShowBottomUI()
        {
            LeanTween.alphaCanvas(bottomUI, 1f, .5f);
        }

        public void ShowTutorial()
        {
            Pause();
            tutorialController.SetOnCompleteTutorialCallback(Pause);
            tutorialController.ShowTutorial();
        }
        
        private void ShowTopUI()
        {
            LeanTween.alphaCanvas(topUI, 1f, .5f);
        }
        private void UpdatePetStatus()
        {
            //Update hunger
            if( activePet.hunger.currentTimer > 0){
                 activePet.hunger.currentTimer--;
            }
            if( activePet.hunger.currentTimer <= 0){
                activePet.hunger.value--;
                if (activePet.sleeping && activePet.hunger.value < activePet.hunger.alert)
                {
                    activePet.hunger.value = activePet.hunger.alert;
                }
                activePet.hunger.currentTimer = statTable.hunger.decayRate;
            }
            
            //Update love
            if( activePet.love.currentTimer > 0){
                 activePet.love.currentTimer--;
            }
            if( activePet.love.currentTimer <= 0 && !activePet.sleeping){
                activePet.love.value--;
                activePet.love.currentTimer = statTable.love.decayRate;
            }
            
            //Update health
            if( activePet.health.currentTimer > 0){
                 activePet.health.currentTimer--;
            }
            if( activePet.health.currentTimer <= 0)
            {
                activePet.health.value = activePet.sleeping ? activePet.health.value + 1 : activePet.health.value - 1;
                if (activePet.health.value > activePet.health.max)
                {
                    activePet.health.value = activePet.health.max;
                }
                activePet.health.currentTimer = statTable.health.decayRate;
            }
            
            //Update dirt
            if( activePet.dirt.currentTimer > 0){
                activePet.dirt.currentTimer--;
             if(activePet.poopOnScreen && !activePet.sleeping)
                 activePet.dirt.currentTimer--;
            }
            if( activePet.dirt.currentTimer <= 0 && !activePet.sleeping){
                activePet.dirt.value--;
                activePet.dirt.currentTimer = statTable.dirt.decayRate;
            }
        }

        private void CheckGameOver()
        {
            /* Game Over priority
             * LOVE
             * HEALTH
             * HUNGER
             * DIRT
             * VICTORY
             */

            if (invincible) return;
            
            if (activePet.love.value <= 0)
            {
                GAMEOVER("LOVE");
                BackgroundController.ShowYouLose();
                MusicController.Play(MusicBg.BadEnding);
                return;
            }
            
            if (activePet.health.value <= 0)
            {
                GAMEOVER("HEALTH");BackgroundController.ShowYouLose();
                MusicController.Play(MusicBg.BadEnding);
                return;
            }
            
            if (activePet.hunger.value <= 0)
            {
                GAMEOVER("HUNGER");
                BackgroundController.ShowYouLose();
                MusicController.Play(MusicBg.BadEnding);
                return;
            }
            
            if (activePet.dirt.value <= 0)
            {
                GAMEOVER("DIRT");
                BackgroundController.ShowYouLose();
                MusicController.Play(MusicBg.BadEnding);
                return;
            }

            if (activePet.YOUWIN)
            {
                GAMEOVER("VICTORY");
                BackgroundController.ShowLevelUp();
                MusicController.Play(MusicBg.GoodEnding);
            }
        }

        private void CheckLevelUp()
        {
            if (tickCounter >= statTable.levelUpThreshold[activePet.lvl])
            {
                LevelUp();
            }
        }

        [Button("Level Up")]
        private void LevelUp()
        {
            activePet.LevelUp();
            if (activePet.lvl > 1)
            {
                MusicController.Play(MusicBg.LevelUp);
                Fader.FadeIn();
                BackgroundController.ShowLevelUp();
            } 
            
            Invoke(nameof(Fader.FadeOut), 30f);
            displayController.curLevel = activePet.lvl;
            displayController.isAlive = activePet.alive;
            UpdateDisplay();
        }

        public void UpdateDisplay()
        {
            displayController.SetCurState(activePet.GetCurrentState());
            displayController.UpdateReferences();
            displayController.UpdateDisplay();
        }
        
        private void UpdateUI()
        {
            statusController.UpdateStatus(
                activePet.health.value,
                activePet.hunger.value,
                activePet.dirt.value,
                activePet.love.value
                );
        }
        
        private void GAMEOVER(string gameOverType)
        {
            activePet.alive = false;
            _ticking = false;
            displayController.SetGameOver(gameOverType);
            MusicController.Play(MusicBg.BadEnding);
            BackgroundController.ShowYouLose();
            Debug.Log("G A M E O V E R");
        }
        
        // testing variables
        [Button("Hatch Egg")]
        public void Hatch()//poipoipoi
        {
            if (activePet.lvl != 0) return;
            
            BackgroundController.ShowIdle();
            
            activePet.alive = true;
            _ticking = true;
            UpdateUI();
            LevelUp();
        }

        [Button("Toggle Autoplay")]
        private void ToggleAutoplay()
        {
            _autoplay = !_autoplay;
        }
        
        [Button("Pause/UnPause")]
        public void Pause()
        {
            Debug.Log("PAUSE CALLED");
            _ticking = !_ticking;
        }

    }    
}