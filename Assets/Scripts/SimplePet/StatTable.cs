using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
namespace MGA.VPet.SimplePet
{
    [CreateAssetMenu (menuName = "VPet/SimplePet/Stat Table", fileName = "StatTable")]
    public class StatTable : ScriptableObject
    {
        [Header("Stats Data")]
        public StatData dirt;
        public StatData health;
        public StatData hunger;
        public StatData love;
        
        [HorizontalLine()]
        
        [Tooltip("86400 seconds in a day")]
        public int ticksPerDay = 500; // 86400; // seconds in a day

         public List<int> levelUpThreshold;
    }

}