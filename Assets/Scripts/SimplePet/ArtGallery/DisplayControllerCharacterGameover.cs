using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MGA.VPet.SimplePet.ArtGallery
{
    public class DisplayControllerCharacterGameover : MonoBehaviour
    {
        [SerializeField] private ArtCollection artCollection;
        [SerializeField] private Image display;
        [SerializeField] private TextMeshProUGUI pieceInfo;
        private int _curIndex = 0;

        private void Start()
        {
            UpdateDisplay();
        }
        
        public void Next()
        {
            _curIndex++;
            if (_curIndex >= artCollection.pieces.Count) _curIndex = 0;
            UpdateDisplay();
        }
        
        public void Prev()
        {
            _curIndex--;
            if (_curIndex < 0) _curIndex =  artCollection.pieces.Count - 1;
            UpdateDisplay();
        }

        private void UpdateDisplay()
        {
            display.sprite = artCollection.pieces[_curIndex].Sprite;
            pieceInfo.text = artCollection.pieces[_curIndex].Description;
        }
    }
}