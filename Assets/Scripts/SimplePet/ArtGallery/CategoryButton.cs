using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MGA.VPet.SimplePet.ArtGallery
{
    public class CategoryButton : MonoBehaviour
    {
        [SerializeField] private GameObject controls;
        [SerializeField] private TextMeshProUGUI label;
        public Button btn;
        [SerializeField] private GameObject artVisualization;
        
        public void EnableCategory()
        {
            btn.interactable = false;
            label.color = Color.black;
            controls.SetActive(true);
            artVisualization.SetActive(true);
        }
        
        public void DisableCategory()
        {
            btn.interactable = true;
            label.color = Color.white;
            controls.SetActive(false);
            artVisualization.SetActive(false);
        }
    }    
}

