using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MGA.VPet.SimplePet.ArtGallery
{
    [CreateAssetMenu(fileName = "Art Collection", menuName = "VPet/SimplePet/ArtCollection")]
    public class ArtCollection : ScriptableObject
    {
        public List<ArtPiece> pieces;
    }

    [Serializable]
    public class ArtPiece
    {
        public string Description = "Sprite";
        public Sprite Sprite;
    }
}