using System.Collections;
using System.Collections.Generic;
using MGA.VPet.Animations;
using TMPro;
using UnityEngine;

namespace MGA.VPet.SimplePet.ArtGallery
{
    public class DisplayControllerCharacterGameplay : MonoBehaviour
    {
        public List<GameObject> Lvls;

        public int curLevel;
        public string curState = "IDLE";
        [SerializeField] private TextMeshProUGUI LevelDisplay;
        [SerializeField] private TextMeshProUGUI StateDisplay;

        private PetAnimationController _petAnimationController;

        private void Start()
        {
            UpdateDisplay();
            UpdateInfo();
        }
        
        public void LevelUp()
        {
            curLevel++;
            if (curLevel >= Lvls.Count)
            {
                curLevel = 0;
            }
            
            if (curLevel == 0 || curLevel == 4)
            {
                UpdateDisplay();
                UpdateInfo();
                return;
            }

            _petAnimationController = Lvls[curLevel].GetComponent<PetAnimationController>();
            _petAnimationController.ChangeState(curState);
            
            UpdateDisplay();
            UpdateInfo();
        }

        public void LevelDown()
        {
            curLevel--;
            if (curLevel < 0)
            {
                curLevel = Lvls.Count - 1;
            }

            if (curLevel == 0 || curLevel == 4)
            {
                UpdateDisplay();
                UpdateInfo();
                return;
            }

            _petAnimationController = Lvls[curLevel].GetComponent<PetAnimationController>();
            _petAnimationController.ChangeState(curState);
            
            UpdateDisplay();
            UpdateInfo();
        }

        public void UpdateDisplay()
        {
            for (var i = 0; i < Lvls.Count; i++)
            {
                 Lvls[i].SetActive(i == curLevel);
            }
        }

        private void UpdateInfo()
        {
            if(curLevel == 0 || curLevel == 4) 
                curState = "IDLE";
            else
            {
                curState = _petAnimationController.GetCurState();
            }
            
            LevelDisplay.text = curLevel.ToString();
            StateDisplay.text = curState;
        }

        public void NextState()
        {
            if (curLevel == 0 || curLevel == 4) return;
            
            _petAnimationController.NextState();
            UpdateInfo();
        }
        
        public void PrevState()
        {
            if (curLevel == 0 || curLevel == 4) return;
            
            _petAnimationController.PrevState();
            UpdateInfo();
        }
    }
}