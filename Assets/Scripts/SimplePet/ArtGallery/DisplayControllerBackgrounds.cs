using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MGA.VPet.SimplePet.ArtGallery
{
    public class DisplayControllerBackgrounds : MonoBehaviour
    {
        [SerializeField] private List<BackgroundData> backgrounds;
        [SerializeField] private TextMeshProUGUI pieceInfo;
        private int _curIndex = 0;

        private void Start()
        {
            UpdateDisplay();
        }
        
        public void Next()
        {
            backgrounds[_curIndex].Object.SetActive(false);
            _curIndex++;
            if (_curIndex >= backgrounds.Count) _curIndex = 0;
            UpdateDisplay();
        }
        
        public void Prev()
        {
            backgrounds[_curIndex].Object.SetActive(false);
            _curIndex--;
            if (_curIndex < 0) _curIndex =  backgrounds.Count - 1;
            UpdateDisplay();
        }

        private void UpdateDisplay()
        {
            backgrounds[_curIndex].Object.SetActive(true);
            pieceInfo.text = backgrounds[_curIndex].Description;
        }
        
    }

    [Serializable]
    public class BackgroundData
    {
        public GameObject Object;
        public string Description;
    }
}
