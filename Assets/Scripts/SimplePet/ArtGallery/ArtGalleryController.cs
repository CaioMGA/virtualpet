using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MGA.VPet.SimplePet.ArtGallery
{
    public class ArtGalleryController : MonoBehaviour
    {
        [SerializeField] private List<CategoryButton> categories;

        private void Start()
        {
            for (var i = 0; i < categories.Count; i++)
            {
                var index = i;
                categories[i].btn.onClick.AddListener(() =>
                {
                    ShowCategoryByIndex(index);
                });
            }
        }

        private void ShowCategoryByIndex(int index)
        {
            for (var i = 0; i < categories.Count; i++)
            {
                if (i == index)
                {
                    categories[i].EnableCategory();
                }
                else
                {
                    categories[i].DisableCategory();
                }
            }
        }

        public void GoToTitleScreen()
        {
            SceneManager.LoadScene("TitleScreen");
        }
    }

}


