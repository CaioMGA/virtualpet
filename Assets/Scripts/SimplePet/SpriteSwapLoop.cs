using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using UnityEngine.Playables;

namespace MGA.VPet
{
    [Serializable]
    public class FrameData
    {
        public float Duration = 0f;
        public Sprite Sprite;
    }
    public class SpriteSwapLoop : MonoBehaviour
    {
        [SerializeField] private Image target;
        public List<FrameData> sprites;
        public  float defaultFrameInterval = 2f;

        public bool Enabled() { return true; }

        public bool NotEnabled => false;
        
        public bool pingPong = false;
        public bool playOnAwake = true;
        
        private int _curFrame = 0;
        private int _frameStep = 1;
        private int  _loopController = -1;

        private void Awake()
        {
            if (playOnAwake)
            {
                Animate();
            }
        }

        public void Animate()
        {
            if (_loopController != -1)
            {
                Stop();
            }

            var frame = sprites[_curFrame];
            target.sprite = frame.Sprite;
            var frameDuration = frame.Duration <= 0 ? defaultFrameInterval : frame.Duration;
            _loopController = LeanTween.value(0f, 1f, frameDuration)
                .setOnComplete(_ =>
                {
                    _curFrame += _frameStep;

                    if (_curFrame >= sprites.Count)
                    {
                        if (pingPong)
                        {
                            _frameStep *= -1;
                            _curFrame += _frameStep;
                        }
                        else
                        {
                            _curFrame = 0;
                        }
                    }
                    else if (_curFrame < 0)
                    {
                        if (pingPong)
                        {
                            _frameStep *= -1;
                            _curFrame += _frameStep;
                        }
                        else
                        {
                            _curFrame = sprites.Count - 1;
                        }
                    }

                    Animate();
                }).id;
        }

        public void Stop()
        {
            LeanTween.cancel(_loopController);
            _loopController = -1;
        }

        private void OnDisable()
        {
            Stop();
        }

        private void OnEnable()
        {
            if (playOnAwake)
            {
                _curFrame = 0;
                Animate();
            }
        }
    }
    
}
