using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MGA.VPet.SimplePet
{
    [Serializable]
    public class StatData
    {
        public string name;
        public int max = 10;
        public int alert = 3;
        public int decayRate = 100;
        public int value = 5;
        public int initialValue = 5;
        public int currentTimer = 0;

        public void Init()
        {
            value = initialValue;
            currentTimer = decayRate;
        }
    }
}
