using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using UnityEngine.PlayerLoop;

namespace MGA.VPet.SimplePet
{
    [Serializable]
    public class PetSpriteCollection
    {
        public Sprite idle;
        public Sprite dirty;
        public Sprite hungry;
        public Sprite loveless;
        public Sprite sick;
        public Sprite sleeping;
        
    }

    [Serializable]
    public class GameOverSprites
    {
        public Sprite DeathByHunger;
        public Sprite DeathByDirt;
        public Sprite DeathByHealth;
        public Sprite DeathByLove;

        public Sprite Victory;

    }
    public class PetSpriteController : MonoBehaviour
    {
        public List<PetSpriteCollection> gameplaySprites;
        public GameOverSprites GameOverSprites;

        [SerializeField] private Image Pet;
        public int lvl;
        [SerializeField] private bool alive = true;
        
        [SerializeField] private int currentState = 0;
        [SerializeField] private int gameOverType = 0;
        
        #region Class Methods

        public void UpdateVisuals()
        {
            var spriteCollection = gameplaySprites[lvl];
            Sprite curSprite = null;
            if (alive)
            {
                switch (currentState)
                {
                    case 1:
                        curSprite = spriteCollection.dirty;
                        break;
                    case 2:
                        curSprite = spriteCollection.hungry;
                        break;
                    case 3:
                        curSprite = spriteCollection.loveless;
                        break;
                    case 4:
                        curSprite = spriteCollection.sick;
                        break;
                    case 5:
                        curSprite = spriteCollection.sleeping;
                        break;
                    case 0:
                    default:
                        curSprite = spriteCollection.idle;
                        break;
                }
            }
            else
            {
                switch (gameOverType)
                {
                    case 1:
                        curSprite = GameOverSprites.DeathByDirt;
                        break;
                    case 2:
                        curSprite = GameOverSprites.DeathByHunger;
                        break;
                    case 3:
                        curSprite = GameOverSprites.DeathByHealth;
                        break;
                    case 4:
                        curSprite = GameOverSprites.DeathByLove;
                        break;
                    
                    default:
                    case 0:
                        curSprite = GameOverSprites.Victory;
                        break;
                }
            }
            Pet.sprite = curSprite;
        }
        #endregion
        
        #region testing

        [Button("LevelUp")]
        private void LevelUp()
        {
            lvl++;
            UpdateVisuals();
        }
        [Button("LevelDown")]
        private void LevelDown()
        {
            lvl--;
            UpdateVisuals();
        }
        [Button("CycleStates")]
        private void CycleStates()
        {
            /* States
             *
             * 0 - idle
             * 1 - dirty
             * 2 - hungry
             * 3 - loveless
             * 4 - sick
             * 5 - sleeping
             */

            currentState++;
            if (currentState >= gameplaySprites.Count)
            {
                currentState = 0;
            }

            UpdateVisuals();
        }
        
        [Button("CycleGameOvers")]
        private void CycleGameOvers()
        {
            /* States
             *
             * 0 - alive
             * 1 - dirty
             * 2 - hungry
             * 3 - loveless
             * 4 - sick
             */

            gameOverType++;
            if (gameOverType >= 5)
            {
                gameOverType = 0;
                alive = true;
            }
            else
            {
                alive = false;
            }

            UpdateVisuals();
        }
        #endregion
    }
    
}
