﻿# Virtual Pet v0.1

Virtual pet game by Caio MGA

# Prints
![Print 0](https://bitbucket.org/CaioMGA/virtualpet/downloads/print_0.jpg)
![Print 1](https://bitbucket.org/CaioMGA/virtualpet/downloads/print_1.jpg)
![Print 2](https://bitbucket.org/CaioMGA/virtualpet/downloads/print_2.jpg)
![Print 3](https://bitbucket.org/CaioMGA/virtualpet/downloads/print_3.jpg)
![Print 4](https://bitbucket.org/CaioMGA/virtualpet/downloads/print_4.jpg)
![Print 5](https://bitbucket.org/CaioMGA/virtualpet/downloads/print_5.jpg)
![Print 6](https://bitbucket.org/CaioMGA/virtualpet/downloads/print_6.jpg)
![Print 7](https://bitbucket.org/CaioMGA/virtualpet/downloads/print_7.jpg)
![Print 8](https://bitbucket.org/CaioMGA/virtualpet/downloads/print_8.jpg)
![Print 9](https://bitbucket.org/CaioMGA/virtualpet/downloads/print_9.jpg)


# Pets
Each pet has its own progression, endings and activities
## Simple Pet
### Endings
5 possible endings:

1. GOOD ENDING:

    Pet passes through all 4 stages of life and reaches adulthood
 
2. HUNGRY ENDING:

    Pet dies of hunger (sad angel)
 
3. SICK ENDING:

    Pet dies of health issues (bat demon)
 
4. LOVELESS ENDING:

     Pet dies of love issues (crying angel)
 
5. DIRT ENDING:

    Pet dies of dirt issues (stinky demon)

### Variables
1. Hunger
2. Health
3. Love
4. Dirt
5. Alive
### Timers
Timers are count down variables that call an event if they end their counting

1. Hunger Timer
    - Triggered by Hunger variable
2. Sick Timer
    - Triggered by Health variable (Zeroed)
    - Triggered randomly twice per game run
3. Loveless Timer
    - Triggered by Love variable
4. Dirt Timer
    - Triggered by Dirt variable
5. Poo Damage Timer
    - Decreases Health over time
    - Triggered randomly twice a day
6. Sleep Timer
    - Occurs during pet sleep


### Activities
Activities alter the pet variables

1. Pet(caress)
    - Increases Love
2. Play
    - Increases Dirt
    - Increases Health
    - Increases Hunger
3. Clean
    - Disable Poo Collection Timer
    - Disable Poo Damage Timer
4. Wash
    - Increases Health
    - ZEROES Dirt variable
4. Give Food
    - Decreases Hunger
    - Increases Dirt (if hunger is ZERO)
    - Increases Health (if health is not MAX)
5. Vaccinate
    - MAXES Health


#IDEAS
- DINOSAURS

- POPE (faith as a variable)
    bad ending: Martin Luther

- MONSTERS

- BABY (PERSON)

- COMPANY (MANAGER)

- COUNTRY

- ARTIST

- Weather

- Day/Night Cycle

- Minigames instead of simple events (to change pet variables)